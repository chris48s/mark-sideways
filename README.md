# mark-sideways

[![PyPI Version](https://img.shields.io/pypi/v/mark-sideways.svg)](https://pypi.org/project/mark-sideways/)
![License](https://img.shields.io/pypi/l/mark-sideways.svg)
![Python Support](https://img.shields.io/pypi/pyversions/mark-sideways.svg)
![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)

Render markdown in the terminal

```sh
mark up example.md        # display markdown code rendered
mark down example.md      # display markdown code with syntax highlighting
mark sideways example.md  # display code and markdown side-by-side
```
